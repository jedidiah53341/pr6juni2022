import React, { useState } from 'react';
import {
    Typography,
    Button,
    TextField,
    List,
    ListItem,
    ListItemText,
    IconButton
} from '@mui/material';

import SearchIcon from '@mui/icons-material/Search';
import ModeEditIcon from '@mui/icons-material/ModeEdit';

import MyDialog from '../component/Dialog'
import EditDialog from '../component/EditDialog'


function MyPage() {
    const [open, setOpen] = useState(false);
    const [editDialogOpen, setEditDialog] = useState(false);
    const [originalText, setOriginalText] = useState('test');
    const [query, setQuery] = useState('');
    const [filteredList, setFilteredList] = useState([]);
    const [list, setList] = useState([
        {
            id: 848383,
            title: 'First'
        },
        {
            id: 848384,
            title: 'Second'
        },
        {
            id: 848385,
            title: 'Third'
        },
        {
            id: 848386,
            title: 'Fourth'
        },
    ]);

    const filterList = () => {
        let tempList = [...list];

        let tempList2 = tempList.filter(post => {
            if (post['title'].toLowerCase().includes(query.toLowerCase())) {
              //returns filtered array
              return post;
            }

          });
          setFilteredList(tempList2);

          console.log(filteredList);
          console.log(list);
    }

    const renderList = (item, index) => {
        return (
            <ListItem
                secondaryAction={
                    <IconButton edge="end"  onClick={() => {setOriginalText(item.title);setEditDialog(true); }} >
                        <ModeEditIcon />
                    </IconButton>
                }
            >
                <ListItemText
                    primary={item.title}
                />
            </ListItem>
        )
    }

    return (
        <div>
            <Typography style={{ textAlign: 'center' }}>MY FIRST LISTING</Typography>
            <div style={{ borderBlock: '1px solid black' }}></div>
            <div style={{ padding: '10px' }}>
                <div style={{ display: 'flex' }}>
                    <TextField
                        value={query}
                        onChange={(e) => {setQuery(e.target.value);filterList()}}
                        id="input-with-icon-textfield"
                        label="Searchbar"
                        InputProps={{
                            endAdornment: <SearchIcon />,
                        }}
                        variant="outlined"
                        style={{ display: 'flex', flexGrow: 1, marginRight: '10px' }}
                    />

                    <Button variant='contained' color='secondary' onClick={() => {setFilteredList([]); setQuery('');}} style={{ width: '120px', marginRight:'2%' }}>Reset Search</Button>

                    <Button variant='contained' color='primary' onClick={() => setOpen(true)} style={{ width: '120px' }}>Add</Button>

                </div>
            </div>

            { filteredList.length === 0 ?
            
            <List dense={true} >
                {list.map((item, index) => renderList(item, index)
                )}
            </List >
            
            :  
            
            <List dense={true} >
            {filteredList.map((item, index) => renderList(item, index)
            )}
            </List >

            
            }

            <MyDialog
                open={open}
                onClose={() => setOpen(false)}
                onAdd={(item) => {
                    let tempList = [...list];
                    tempList.push(item);
                    setList(tempList);
                }}
            />

            <EditDialog
                editDialogOpen={editDialogOpen}
                editDialogClose={() => {setEditDialog(false); setOriginalText('');}}
                originalText={originalText}
                onEdit={(item) => {
                    let tempList = [...list];
                    for (let i=0; i<tempList.length; i++) {
                        if(tempList[i]['title'] === originalText) {
                            tempList[i]['title'] = item['title'];
                        }
                    }
                    setList(tempList);
                }}
            />              
        </div >
    );
}
export default MyPage;
