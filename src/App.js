import './App.css';
import React from 'react';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { green } from '@mui/material/colors';

import MyPage from './screen/MyPage';

const theme = createTheme({
    palette: {
        primary: {
            main: green[500]
        }
    }
});

function App() {
    return (
        <ThemeProvider theme={theme}>
            <MyPage />
        </ThemeProvider>
    );
}
export default App;
