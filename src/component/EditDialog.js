import React, { useState } from 'react';
import {
    Typography,
    Button,
    Dialog,
    TextField
} from '@mui/material';

function EditDialog(props = {
    editDialogOpen: false,
    originalText: '',
    editDialogClose: () => { },
    onEdit: () => { }
}) {
    const [title, setTitle] = useState('');
    const [originalTitle, setOriginalTitle] = useState(props.originalText);

    return (
        <div>
            <Dialog
                open={props.editDialogOpen}
                onClose={props.editDialogClose}
            >
                <div style={{ padding: '10px' }}>
                    <Typography>Edit Item</Typography>

                    Change from :<br/>
                    <TextField
                        disabled
                        value={props.originalText}
                        onChange={(e) => setTitle(e.target.value)}
                        style={{ marginTop: '10px', marginBottom: '10px' }}
                    />

                    <br/><br/>
                    To :<br/>
                    <TextField
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                        style={{ marginTop: '10px', marginBottom: '10px' }}
                    />

                    <br />

                    <Button
                        variant="contained"
                        fullWidth
                        onClick={() => {
                            // let tempList = [...list];
                            // tempList.push({ title: title });
                            // setList(tempList);

                            props.onEdit({
                                title: title
                            });
                            setTitle('');
                            props.editDialogClose()
                        }}
                    >Apply Edit</Button>
                </div>
            </Dialog>
        </div>
    );
}
export default EditDialog;
