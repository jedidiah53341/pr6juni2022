import React, { useState, useEffect } from 'react';
import {
    Box,
    Typography,
    Button,
    Dialog,
    TextField
} from '@mui/material';

function MyDialog(props = {
    open: false,
    onClose: () => { },
    onAdd: () => { }
}) {
    const [title, setTitle] = useState('');

    return (
        <div>
            <Dialog
                open={props.open}
                onClose={props.onClose}
            >
                <div style={{ padding: '10px' }}>
                    <Typography>Add Item</Typography>

                    <TextField
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                        style={{ marginTop: '10px', marginBottom: '10px' }}
                    />

                    <br />

                    <Button
                        variant="contained"
                        fullWidth
                        onClick={() => {
                            // let tempList = [...list];
                            // tempList.push({ title: title });
                            // setList(tempList);

                            props.onAdd({
                                title: title
                            });
                            props.onClose()
                        }}
                    >Save</Button>
                </div>
            </Dialog>
        </div>
    );
}
export default MyDialog;
